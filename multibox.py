from Bot import Bot
from dotenv import load_dotenv
from multiprocessing import Process, Lock, shared_memory, Queue, Pipe, freeze_support
from time import sleep
import os, keyboard, time, json, logging, argparse, sys
import numpy as np
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import tensorflow as tf

def secCheckDetector(sensitivity, queue, pipes, lock, logLevel):
    # Configure working directory path
    currentDirectory = os.path.curdir
    fullPath = os.path.abspath(currentDirectory)
    logDir = fullPath + os.path.sep + 'logs' + os.path.sep
    logFile = logDir + 'secCheckDetector.log'
    modelDir = fullPath + os.path.sep + 'model'

    logging.basicConfig(filename=logFile, encoding='utf-8', format='%(levelname)s %(asctime)s %(lineno)s %(message)s', level=logLevel)
    def Log(message, logLevel=logging.INFO):
        with lock:
            print(f'[MASTER - Security Check Detector] - {message}')
            if logLevel == logging.DEBUG:
                logging.debug(f'[MASTER - Security Check Detector] - {message}')
            elif logLevel == logging.INFO:
                logging.info(f'[MASTER - Security Check Detector] - {message}')
            elif logLevel == logging.WARNING:
                logging.warning(f'[MASTER - Security Check Detector] - {message}')
            elif logLevel == logging.ERROR:
                logging.error(f'[MASTER - Security Check Detector] - {message}')
            elif logLevel == logging.CRITICAL:
                logging.critical(f'[MASTER - Security Check Detector] - {message}')

    Log('Loading shared memory')
    shm = shared_memory.SharedMemory(name='BotStatus')

    # Check if security checks are set to fail mode
    if shm.buf[5] == 0x01:
        Log('Fail mode enabled', logging.INFO)

    Log('Loading model')
    startTime = time.perf_counter()
    model = tf.saved_model.load(modelDir)
    endTime = time.perf_counter()
    Log(f'Took {endTime - startTime} seconds to load!')

    Log('Initializing detection function')
    startTime = time.perf_counter()
    detect_fn = model.signatures['serving_default']
    endTime = time.perf_counter()
    Log(f'Took {endTime - startTime} seconds to initialize!')

    Log('Clearing security check initialization bit', logging.DEBUG)
    shm.buf[4] = 0x00

    Log('Beginning detection loop')
    while shm.buf[0] == 0x01:
        try:
            if not queue.empty():
                Log('Polling queue for customer')
                botNum, image = queue.get()

                Log(f'Got customer: {botNum}')
                Log('Accessing data pipe')
                pipe = pipes[botNum]

                Log('Detecting objects')
                startTime = time.perf_counter()
                detections = detect_fn(image)
                endTime = time.perf_counter()

                Log(f'Took {endTime - startTime} seconds to detect!')
                num_detections = int(detections.pop('num_detections'))

                Log(f'Detected {num_detections} total objects!')
                detections = {key: value[0, :num_detections].numpy()
                            for key, value in detections.items()}
                detections['num_detections'] = num_detections
                detections['detection_classes'] = detections['detection_classes'].astype(np.int64)
                answer = 0

                Log(f'Parsing detection scores: {detections["detection_scores"]}')
                for score in detections['detection_scores']:
                    if score > sensitivity:
                        answer += 1
                if answer == 0:
                    Log('Failed to detect any objects! Defaulting to 1!', logging.WARNING)
                    answer = 1
                elif answer > 9:
                    Log('Detected false positives! Defaulting to 9!', logging.WARNING)
                    answer = 9
                else:
                    Log(f'Detected {answer} objects!')

                if shm.buf[5]:
                    Log('Setting intentional wrong answer')
                    answer = (answer + 1) % 9 + 1

                Log(f'Sending response answer: {answer}')
                pipe.send(answer)
        except Exception as err:
            Log(f'Error in detection loop - {err}', logging.CRITICAL)
            raise err

def periodic(lock):
    def Log(message, logLevel=logging.INFO):
        pid = os.getpid()
        with lock:
            print(f'[{pid} - Periodic] - {message}')
            if logLevel == logging.DEBUG:
                logging.debug(f'[{pid} - Periodic] - {message}')
            elif logLevel == logging.INFO:
                logging.info(f'[{pid} - Periodic] - {message}')
            elif logLevel == logging.WARNING:
                logging.warning(f'[{pid} - Periodic] - {message}')
            elif logLevel == logging.ERROR:
                logging.error(f'[{pid} - Periodic] - {message}')
            elif logLevel == logging.CRITICAL:
                logging.critical(f'[{pid} - Periodic] - {message}')

    if shm.buf[1]:
        # Delay between each action just in case
        try:
            bot.Delay()
            Log('Attempting to find mob', logging.DEBUG)
            bot.FindMob()
        except Exception as err:
            Log(f'Error finding mob: {err}', logging.ERROR)
            pass
        try:
            bot.Delay()
            Log('Attempting to kill mob', logging.DEBUG)
            bot.KillMob()
        except Exception as err:
            Log(f'Error killing mob: {err}', logging.ERROR)
            pass
        try:
            bot.Delay()
            Log('Attempting to train stat', logging.DEBUG)
            bot.TrainStat()
        except Exception as err:
            Log(f'Error training stat: {err}', logging.ERROR)
            pass
        try:
            bot.Delay()
            Log('Checking for security check', logging.DEBUG)
            bot.DetectSecCheck(shm.buf[5])
        except Exception as err:
            Log(f'Error detecting security check: {err}', logging.ERROR)
            pass
        try:
            bot.Delay()
            Log('Attempting to process new chat log', logging.DEBUG)
            bot.ProcessNewChat()
        except Exception as err:
            Log(f'Error processing chat lines: {err}', logging.ERROR)
            pass
    elif shm.buf[3]:
        Log('Relogging account')
        with lock:
            shm.buf[3] -= 0x01
            sleep(shm.buf[3])
            bot.Relog()

def run(account, password, mob, delayAggression, mobList, ashList, botNumber, lock, queue, pipe, logLevel):
    # Configure working directory path
    currentDirectory = os.path.curdir
    fullPath = os.path.abspath(currentDirectory)
    logDir = fullPath + os.path.sep + 'logs' + os.path.sep
    logFile = logDir + account + '.log'
    logging.basicConfig(filename=logFile, encoding='utf-8', format='%(levelname)s %(asctime)s %(lineno)s %(message)s', level=logLevel)

    def Log(message, logLevel=logging.INFO):
        with lock:
            print(f'[MASTER - {account}] - {message}')
            if logLevel == logging.DEBUG:
                logging.debug(f'[MASTER - {account}] - {message}')
            elif logLevel == logging.INFO:
                logging.info(f'[MASTER - {account}] - {message}')
            elif logLevel == logging.WARNING:
                logging.warning(f'[MASTER - {account}] - {message}')
            elif logLevel == logging.ERROR:
                logging.error(f'[MASTER - {account}] - {message}')
            elif logLevel == logging.CRITICAL:
                logging.critical(f'[MASTER - {account}] - {message}')
    try:
        Log('Configuring shared memory')
        global shm
        shm = shared_memory.SharedMemory(name='BotStatus')

        Log('Configuring bot')
        global bot
        bot = Bot(botNumber, account, password, mob, delayAggression, mobList, ashList, lock, queue, pipe, logLevel)
        Log('Logging in bot')
        bot.Login()
        Log('Configuring bot session')
        bot.ConfigureSession()
        # Log('Positioning bot window.)
        # colNumber = botNumber % 9
        # rowNumber = botNumber // 9
        # size = (426, 200)
        # position = (size[0] * colNumber, rowNumber * size[1])
        # Log(f'Setting Position to {position}', logging.DEBUG)
        # bot.SetWindow(position=position, size=size)

        Log('Waiting for bot detector')
        while shm.buf[4] == 0x01:
            sleep(1)

        Log('Beginning bot loop')
        while shm.buf[0] == 0x01:
            periodic(lock)

        Log('Closing bot driver')
        bot.Driver.close()
    except Exception as err:
        Log(f'Bot crash detected - {err}', logging.ERROR)
        try:
          bot.Driver.close()
        except Exception as err:
            Log(f'Error cleaning up - {err}', logging.ERROR)
            raise

def Log(message, logLevel=logging.INFO):
    with lock:
        print(f'[MASTER] - {message}')
        if logLevel == logging.DEBUG:
            logging.debug(f'[MASTER] - {message}')
        elif logLevel == logging.INFO:
            logging.info(f'[MASTER] - {message}')
        elif logLevel == logging.WARNING:
            logging.warning(f'[MASTER] - {message}')
        elif logLevel == logging.ERROR:
            logging.error(f'[MASTER] - {message}')
        elif logLevel == logging.CRITICAL:
            logging.critical(f'[MASTER] - {message}')

def keyboard_callback(attr):
    global shm

    if attr.name == 'scroll lock':
        Log(f'Processing keypress {attr}')
        shm.buf[0] = 0x00
        Log('Quitting')
    elif attr.name == 'delete':
        Log(f'Processing keypress {attr}')
        shm.buf[1] = 0x01 ^ shm.buf[1]
        Log(f'Status: {bool(shm.buf[1])}')
    elif attr.name == 'insert':
        Log(f'Processing keypress {attr}')
        shm.buf[2] = 0x01 ^ shm.buf[2]
        Log(f'Training: {bool(shm.buf[2])}')
    elif attr.name == 'print screen':
        Log(f'Processing keypress {attr}')
        shm.buf[3] = 0x01 * len(os.environ['ACCOUNTS'].split(';'))
        Log('Relogging all bots')

def main(sensitivity,delayAggression,failSecurityChecks,logLevel):
    Log('Acquiring lock')

    Log('Configuring shared memory')
    global shm
    shm = shared_memory.SharedMemory(create=True, name='BotStatus', size=10)

    Log('Setting program lifecycle flag')
    shm.buf[0] = 0x01

    Log('Setting security check initialization flag')
    shm.buf[4] = 0x01

    Log('Setting security check enable/disable flag')
    if failSecurityChecks:
        shm.buf[5] = 0x01
    else:
        shm.buf[5] = 0x00

    Log('Configuring keyboard callback')
    keyboard.on_press(keyboard_callback)

    Log('Configuring account environment')
    accounts = []
    passwords = []
    mobs = []

    Log('Loading environment variables')
    load_dotenv()

    Log('Splitting account list')
    accounts = os.environ['ACCOUNTS'].split(';')
    passwords = os.environ['PASSWORDS'].split(';')
    mobs = [str(mob) for mob in os.environ['MOBS'].split(';')]

    Log('Loading ash list')
    with open('./ashList.json') as ashListFile:
        ashList = json.load(ashListFile)
    Log(f'Ash list loaded: {ashList}')

    Log('Loading mob list')
    with open('./mobs.json') as mobFile:
        mobList = json.load(mobFile)
    Log(f'Mob list loaded: {mobList}')

    Log(f'Spinning up {len(accounts)} bots')
    bots = []
    detPipes = []
    botPipes = []

    Log('Initializing queue')
    queue = Queue()

    Log('Processing account list')
    for account, password, mob in zip(accounts, passwords, mobs):
        Log(f'Creating {account}')
        Log('Creating data pipe')
        detPipe, botPipe = Pipe(duplex=True)

        Log('Initializing bot process')
        process = Process(target=run, args=(account,password,mob,delayAggression,mobList,ashList,accounts.index(account),lock,queue,botPipe,logLevel,), name=account)

        Log('Starting bot process')
        process.start()
        bots.append(process)
        detPipes.append(detPipe)
        botPipes.append(botPipe)

    Log('Configuring bot detector')
    detector = Process(target=secCheckDetector, args=(sensitivity,queue,detPipes,lock,logLevel,))
    detector.start()

    Log('Beginning bot monitor')
    while shm.buf[0] == 0x01:
        for bot in bots:
            if not bot.is_alive():
                Log(f'Found dead bot at {bot.pid}:{bot.name}')
                Log('Reviving bot')
                Log('Terminating old bot')
                bot.terminate()
                bots.remove(bot)

                Log('Setting account details')
                account = bot.name
                password = passwords[accounts.index(account)]
                mob = mobs[accounts.index(account)]

                Log('Initializing bot process')
                bot = Process(target=run, args=(account,password,mob,delayAggression,mobList,ashList,accounts.index(account),lock,queue,botPipes[accounts.index(account)],logLevel,), name=account)

                Log('Starting bot process')
                bot.start()
                bots.append(bot)
        sleep(1)

if __name__ == '__main__':
    # Enable multiprocessing freeze support
    freeze_support()

    # Setup a lock object
    lock = Lock()

    # Configure working directory path
    currentDirectory = os.path.curdir
    fullPath = os.path.abspath(currentDirectory)
    logDir = fullPath + os.path.sep + 'logs' + os.path.sep
    logFile = logDir + 'master.log'

    # Parse arguments
    parser = argparse.ArgumentParser(description='Multibox argument parser')
    parser.add_argument('--log', '-l', help='Set the log level for debugging. Valid options are: DEBUG, INFO, WARNING, ERROR, CRITICAL. Defaults to INFO.', type=str)
    parser.add_argument('--aggression', '-a', help='Set the delay aggression as a percentage. Higher values means higher aggression. Valid options are: 0 to 100. Defaults to 0.', type=int)
    parser.add_argument('--sensitivity', '-s', help='Set the sensitivity of the security check detector. This value translates to how confident a detection needs to be to be counted (e.g. 90% sure). Valid options are: 0.00 to 1.00. Defaults to 0.90.', type=float)
    parser.add_argument('--fail', '-f', help='Fail all security checks on purpose.', action='store_true')
    try:
        args = parser.parse_args()
    except Exception as err:
        logging.basicConfig(filename=logFile, encoding='utf-8', format='%(levelname)s %(asctime)s %(lineno)s %(message)s', level=logging.INFO)
        Log('Unable to process arguments', logging.CRITICAL)
        parser.print_help()
        sys.exit(1)

    # Set log level based on arguments if necessary, or default to INFO
    if args.log:
        if args.log == 'DEBUG':
            logLevel = logging.DEBUG
        elif args.log == 'INFO':
            logLevel = logging.INFO
        elif args.log == 'WARNING':
            logLevel = logging.WARNING
        elif args.log == 'ERROR':
            logLevel = logging.ERROR
        elif args.log == 'CRITICAL':
            logLevel = logging.CRITICAL
        else:
            logging.basicConfig(filename=logFile, encoding='utf-8', format='%(levelname)s %(asctime)s %(lineno)s %(message)s', level=logging.INFO)
            Log('Unable to process arguments', logging.CRITICAL)
            parser.print_help()
            sys.exit(1)
    else:
        logLevel = logging.INFO
    
    # Set delay aggression based on arguments if necessary, or default to 0
    delayAggression = 0
    if args.aggression:
        try:
            delayAggression = args.aggression
            if delayAggression > 10:
                Log(f'High aggression may lead to severe timeout issues!', logging.WARNING)
        except Exception as err:
            Log(f'Unable to process argument for aggression - {err}', logging.CRITICAL)
            parser.print_help()
            sys.exit(2)

    # Set security check sensitivity based on arguments if necessary, or default to 90%
    sensitivity = 0.90
    if args.sensitivity:
        try:
            sensitivity = args.sensitivity
            if sensitivity <= 0.85:
                Log(f'High sensitivity may lead to false positives which means more failed security checks!', logging.WARNING)
        except Exception as err:
            Log(f'Unable to process argument for sensitivity - {err}', logging.CRITICAL)
            parser.print_help()
            sys.exit(3)

    # Disable security check detector
    failSecurityChecks = False
    if args.fail:
        try:
            failSecurityChecks = args.fail
        except Exception as err:
            Log(f'Unable to process argument for disabling security checks - {err}', logging.CRITICAL)
            parser.print_help()
            sys.exit(4)

    # Ensure the log directory exists
    if not os.path.exists('logs'):
        os.makedirs('logs')

    # Configure the logging for the master
    logging.basicConfig(filename=logFile, encoding='utf-8', format='%(levelname)s %(asctime)s %(lineno)s %(message)s', level=logLevel)

    Log('Entering process main')
    main(sensitivity,delayAggression,failSecurityChecks,logLevel)
