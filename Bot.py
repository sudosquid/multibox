import os, requests, logging
import numpy as np
import selenium.webdriver as webdriver
from io import BytesIO
from PIL import Image
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from time import sleep
from tensorflow import convert_to_tensor, newaxis

class Bot:
    URL = 'https://rwk2.racewarkingdoms.com'
    TITLE = 'Race War Kingdoms'

    def Log(self, message, logLevel=logging.INFO):
        with self.Lock:
            print(f'[{self.PID} - {self.Account}] - {message}')
            if logLevel == logging.DEBUG:
                logging.debug(f'[{self.PID} - {self.Account}] - {message}')
            elif logLevel == logging.INFO:
                logging.info(f'[{self.PID} - {self.Account}] - {message}')
            elif logLevel == logging.WARNING:
                logging.warning(f'[{self.PID} - {self.Account}] - {message}')
            elif logLevel == logging.ERROR:
                logging.error(f'[{self.PID} - {self.Account}] - {message}')
            elif logLevel == logging.CRITICAL:
                logging.critical(f'[{self.PID} - {self.Account}] - {message}')

    def __init__(self, botNumber, account, password, mob, delayAggression, mobList, ashList, lock, queue, pipe, logLevel):
        # Configure working directory path
        currentDirectory = os.path.curdir
        fullPath = os.path.abspath(currentDirectory)
        logDir = fullPath + os.path.sep + 'logs' + os.path.sep
        logFile = logDir + account + '.log'
        logging.basicConfig(filename=logFile, encoding='utf-8', format='%(levelname)s %(asctime)s %(lineno)s %(message)s', level=logLevel)

        self.Lock = lock
        self.PID = os.getpid()
        self.BotNumber = botNumber
        self.Account = account
        self.Password = password
        self.Mob = mob
        self.DelayAggression = delayAggression
        self.MobList = mobList
        self.AshList = ashList
        self.Queue = queue
        self.Pipe = pipe
        self.ChatMessages = []
        self.GCD = 0.05

        self.Log('Bot initialized!')
        self.Log('Configuring firefox profile')
        self.Profile = FirefoxProfile()

        self.Log('Opening browser')
        self.Driver = webdriver.Firefox(firefox_profile=self.Profile, executable_path='drivers/geckodriver.exe')

        self.Log('Loading page')
        self.Driver.get(Bot.URL)

        self.Log('Asserting page title', logging.DEBUG)
        assert Bot.TITLE in self.Driver.title

    def SetWindow(self, position=(-15,0), size=(320,175)):
        self.Log('Configuring window position and size')
        self.Driver.set_window_position(position[0], position[1], windowHandle='current')
        self.Driver.set_window_size(size[0], size[1], windowHandle='current')

        self.Log('Setting browser to fullscreen')
        self.Driver.fullscreen_window()

    def Login(self):
        self.Log('Logging in')
        try:
            self.Log('Finding username field')
            unFields = self.Driver.find_elements(By.XPATH, "//input[@type='text' and @name='login']")

            self.Log('Filling in username')
            unFields[-1].send_keys(Keys.CONTROL + "a")
            unFields[-1].send_keys(self.Account)
        except Exception as err:
            self.Log('Error finding username field - {err}', logging.CRITICAL)
            raise err

        try:
            self.Log('Finding password field')
            pwFields = self.Driver.find_elements(By.NAME, 'password')

            self.Log('Filling in password')
            pwFields[-1].send_keys(self.Password)
        except Exception as err:
            self.Log('Error finding password field - {err}', logging.CRITICAL)
            raise err

        try:
            loginButton = self.Driver.find_element(By.ID, 'subshit')
            loginButton.click()
        except Exception as err:
            self.Log(f'Error logging in - {err}')
            raise err

        self.Log('Asserting page title', logging.DEBUG)
        sleep(1)
        assert self.Account in self.Driver.title
        self.Log('Successfully logged in!')
    
    def Relog(self):
        self.Log('Reloading page')
        self.Driver.get(Bot.URL)

        self.Log('Asserting page title', logging.DEBUG)
        assert Bot.TITLE in self.Driver.title

        self.Log('Relogging in')
        self.Login()

        self.Log('Reconfiguring session')
        self.ConfigureSession()

    def ConfigureSession(self):
        self.Log('Finding location element')
        self.LocationElement = self.Driver.find_element(By.ID, 's_Loc')

        self.Log('Extracting location parts')
        self.Location = self.LocationElement.text.split(',')
        self.X = self.Location[0]
        self.Plane = self.Location[1]
        self.Y = self.Location[2]

        self.Log('Finding mob id')
        self.MobId = self.MobList[self.Plane][self.Mob]

        self.Log('Finding chat input element')
        self.ChatInput = self.Driver.find_element(By.ID, 'chattybox')

        self.Log('Disabling delay bar')
        self.ChatInput.send_keys('/dis' + Keys.RETURN)

        self.Log('Sleeping for GCD', logging.DEBUG)
        sleep(self.GCD)

        self.Log('Disabling browser warning')
        self.ChatInput.send_keys('/warn' + Keys.RETURN)

        self.Log('Sleeping for GCD')
        sleep(self.GCD)

        self.Log('Configuring actions')
        self.Log('Finding more button')
        self.MoreButton = self.Driver.find_element(By.NAME, 'MoreGeneral')

        self.Log('Clicking more button')
        self.MoreButton.click()

        self.Log('Configuring ash action')
        self.Log('Finding first action and selecting DESTROY')
        td0 = self.Driver.find_element(By.XPATH, "//form[@id='general0']/../td")
        self.Action0 = Select(td0.find_element(By.XPATH, "./select[@name='action']"))
        self.Action0.select_by_visible_text('DESTROY')

        self.Log('Finding first option and button')
        self.Target0 = Select(td0.find_element(By.XPATH, "./select[@name='target']"))
        self.Submit0 = td0.find_element(By.XPATH, "./span[@id='s_subbut0']/input[@src='submit.jpg']")

        self.Log('Configuring find mob action')
        self.Log('Finding second action and selecting Fight')
        td1 = self.Driver.find_element(By.XPATH, "//form[@id='general1']/../td")

        self.Log('Finding first option and button')
        self.Action1 = Select(td1.find_element(By.XPATH, ".//select[@name='action']"))
        self.Target1 = Select(td1.find_element(By.XPATH, "./select[@name='target']"))
        self.Submit1 = td1.find_element(By.XPATH, "./span[@id='s_subbut1']/input[@src='submit.jpg']")

        self.Log(f'Selecting mobId - {self.MobId}')
        self.Target1.select_by_index(self.MobId)

        self.Log('Finding chat messages')
        self.ChatSpan = self.Driver.find_element(By.ID, 's_Chat')

        self.Log('Preprocessing chat messages')
        for chatLine in self.ChatSpan.text.splitlines():
            self.Log(f'Saving chat message {chatLine}', logging.DEBUG)
            self.ChatMessages.append(chatLine)

            # if 'Haste' in chatLine:
            #     self.Haste = int(chatLine.replace(' Haste: ', ''))
            #     self.Log(f'Found haste level {self.Haste}')
            #     self.KillDelay = 130/(self.Haste + 120)
            #     self.TrainDelay = 190/(self.Haste + 250)
            #     self.AshDelay = 190/(self.Haste + 250)
            #     self.Log(f'Set delays: [{self.KillDelay}, {self.TrainDelay}, {self.AshDelay}]')

        self.Log('Finding response messages')
        self.ResponseSpan = self.Driver.find_element(By.ID, 's_Response')

        self.Log('Succesfully configured session!')
    
    def Delay(self):
        self.Log('Waiting for barney bar to have width of 0', logging.DEBUG)
        waiting = True
        while waiting:
            try:
                # Inline everything to try to avoid stale elements in the DOM
                while int(self.Driver.find_element(By.XPATH, "//div[@id='BarneyDiv']//td[@id='BarneyFull']").get_attribute('width').replace('%','')) > self.DelayAggression:
                    sleep(self.GCD)
                self.Log('Done waiting', logging.DEBUG)
                waiting = False
            except StaleElementReferenceException as err:
                # This happens because the DOM keeps refreshing this element to change the width
                # It just means that the delay bar is still being updated, so we are not ready to make actions
                # Continue the waiting loop until it stops updating
                continue
            except Exception as err:
                self.Log(f'Error processing barney bar width - {err}', logging.CRITICAL)
                raise
    
    def FindMob(self):
        # Wrap in a try block so we only sleep if the action was successful
        try:
            self.Log('Locating find button', logging.DEBUG)
            self.Submit1.click()
        except NoSuchElementException as err:
            self.Log(f'Could not locate find button - {err}', logging.ERROR)
            raise
        except Exception as err:
            self.Log(f'Error finding mob - {err}', logging.ERROR)
            raise

    def KillMob(self):
        # Wrap in a try block so we only sleep if the action was successful
        try:
            self.Log('Locating kill button', logging.DEBUG)
            self.Driver.find_element(By.NAME, 'gattackattack').click()
        except NoSuchElementException as err:
            # This could happen if we have no mob to fight
            # For example, a security check happened after clicking Submit to find a mob
            # In such a case, ignore the error but log what happened
            self.Log(f'Could not locate attack button - {err}', logging.DEBUG)
            pass
        except Exception as err:
            self.Log(f'Error killing mob - {err}', logging.ERROR)
            raise

    def AshItem(self):
        # Wrap in a try block so we only sleep if the action was successful
        try:
            self.Log('Locating ash button', logging.DEBUG)
            self.Submit0.click()
        except NoSuchElementException as err:
            # In this case, the session is probably not configured properly
            # Raise the error so we can fix the session
            self.Log(f'Could not locate ash button - {err}', logging.ERROR)
            raise
        except Exception as err:
            # If we failed to ash, don't worry about it
            self.Log(f'Error ashing item - {err}', logging.ERROR)
            pass
    
    def TrainStat(self):
        # Try to train, but pass the exception if not able
        # This is expected to throw if it's not time to train yet
        try:
            self.Log('Locating train button', logging.DEBUG)
            self.Driver.find_element(By.XPATH, "//span[@id='s_FightWin']/img[@width='102' and @height='54']").click()
        except NoSuchElementException as err:
            # In this case, we are not ready to train yet
            # Ignore the error
            self.Log('Skipping training, not needed', logging.INFO)
            self.Log(f'Skipping training, not needed - {err}', logging.DEBUG)
            pass
        except Exception as err:
            self.Log(f'Error training stat - {err}', logging.ERROR)
            raise

    def DetectSecCheck(self):
        while True:
            if 'Bot check. Count the objects seen.' in self.ResponseSpan.text:
                secImageUrl = self.ResponseSpan.find_element(By.XPATH, "./img[@class='mycshift']").get_attribute('src')
                secImage = requests.get(secImageUrl, stream=True)
                self.Log(f'Security check detected: {secImageUrl}')

                self.Log('Detecting objects')
                img = Image.open(BytesIO(secImage.content))
                w, h = img.size
                img = img.resize((w * 5, h * 5), Image.ANTIALIAS)
                img_np = np.array(img)
                input_tensor = convert_to_tensor(img_np)
                input_tensor = input_tensor[newaxis, ...]

                self.Log('Requesting detector resource')
                self.Queue.put((self.BotNumber, input_tensor))

                self.Log('Waiting for response')
                self.Pipe.poll(None)

                self.Log('Got response')
                num_detections = self.Pipe.recv()

                self.Log(f'Detected {num_detections} objects!')
                secAnswerButton = self.Driver.find_element(By.NAME, f'securify{num_detections}')
                if secAnswerButton is not None:
                    self.Log('Successfully found security check answer button!')
                    secAnswerButton.click()
                else:
                    self.Log('Failed to find security check answer button!')
            else:
                break;
            self.Log('Sleeping for GCD', logging.DEBUG)
            sleep(self.GCD)

    def ProcessNewChat(self):
        self.Log('Splitting chat lines and selecting new lines', logging.DEBUG)
        chatLines = self.ChatSpan.text.splitlines()
        newLines = [ chatLine for chatLine in chatLines if chatLine not in self.ChatMessages ]

        for chatLine in newLines:
            self.Log(f'New chat message: {chatLine}')
            self.ChatMessages.append(chatLine)

            if ('You find a' in chatLine):
                item = chatLine.replace(' You find a ', '').replace(' on the enemy\'s corpse! Click to DESTROY.', '')

                self.Log('Checking ash settings', logging.DEBUG)
                shouldAsh = None
                for entry in self.AshList:
                    if entry['Name'] in item:
                        self.Log(f'Found ash entry: {entry["shouldAsh"]}', logging.DEBUG)
                        shouldAsh = entry['shouldAsh']
                        break
                if shouldAsh is None:
                    self.Log(f'Item not found in ash list - {item}', logging.WARNING)
                    self.Log('Defaulting to keep', logging.WARNING)
                    shouldAsh = False

                if shouldAsh:
                    self.Log(f'Ashing item: [{item}]')
                    try:
                        self.Target0.select_by_visible_text(item)
                        self.AshItem()
                    except Exception as err:
                        self.Log(f'Error trying to ash item - {err}')
                        pass
                else:
                    self.Log(f'Keeping item: [{item}]')
                self.Log('Sleeping for GCD', logging.DEBUG)
                sleep(self.GCD)
            elif 'You have not waited the necessary amount of time before doing another action. Now you must wait an additional 10 seconds.' in chatLine:
                self.Log('Detected delay penalty! Waiting 10 seconds to continue')
                self.Delay()
            elif 'You must wait for your first command to be processed or timeout in' in chatLine:
                self.Log('Detected server lag! Increasing GCD by 5%')
                self.GCD *= 1.05
                self.Delay()
